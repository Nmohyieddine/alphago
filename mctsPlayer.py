import Goban
from playerInterface import *


from node import Node, mcts_pred


class myPlayer(PlayerInterface):

    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None

    def getPlayerName(self):
        return "MCTS Player"

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"
        # la liste de toutes les actions legal
        moves = self._board.legal_moves()
        print(f" the posible move {moves}")
        # Création d'un noeud
        node = Node()
        # attribué la nouvelle version du board au state
        node.state = self._board
        # méthode mcts qui permet de donné le coup à jouer
        result = mcts_pred(node,10)
        # éxécuter le mouvement dans le board
        self._board.play_move(result)

        return node.state.flat_to_name(result)

    def playOpponentMove(self, move):
        self._board.push(Goban.Board.name_to_flat(move))

    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")