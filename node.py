import random

import Goban
import copy
from math import sqrt, log, e, inf
class Node:
    def __init__(self):
        self.state = Goban.Board()
        self.action = ''
        self.children = []
        self.parent = None
        self.N = 0
        self.n = 0
        self.v = 0
def ucb1(curr_node):
    ans = curr_node.v+2*(sqrt(log(curr_node.N+e+(10**-6))/(curr_node.n+(10**-10))))
    #ans = curr_node.v + 2 * sqrt(2 * log(curr_node.N + e+(10**-6)) / (curr_node.n + 10**-10))
    return ans

def expand(curr_node):
    if(len(curr_node.children)==0):
        return curr_node

    idx = -1
    max_ucb = -inf
    sel_child = None
    for i in curr_node.children:
        tmp = ucb1(i)
        if(tmp>max_ucb):
            idx = i
            max_ucb = tmp
            sel_child = i

    return(expand(sel_child))


def rollout(curr_node):
    if (curr_node.state.is_game_over()):
        board = curr_node.state
        if (board.result() == '1-0'):
            # print("h1")
            return (1, curr_node)
        elif (board.result() == '0-1'):
            # print("h2")
            return (-1, curr_node)
        else:
            return (0.5, curr_node)

    # TODO : modify the all_moves
    #all_moves = [curr_node.state.san(i) for i in list(curr_node.state.legal_moves)]
    all_moves = curr_node.state.legal_moves()  # legal moves are given as internal (flat) coordinates, not A1, A2, ...
    #all_moves = [curr_node.state.flat_to_name(l) for l in legals]

    for move in all_moves:
        # TODO create temporary state using copy.deepcopy(Board)
        #tmp_state = chess.Board(curr_node.state.fen())
        #tmp_state.push_san(i)
        tmp_state = copy.deepcopy(curr_node.state)
        tmp_state.play_move(move)
        child = Node()
        child.state = tmp_state
        child.parent = curr_node
        curr_node.children.append(child)
    rnd_state = random.choice(list(curr_node.children))

    return rollout(rnd_state)

def rollback(curr_node,reward):
    curr_node.n+=1
    curr_node.v+=reward
    while(curr_node.parent!=None):
        curr_node.N+=1
        curr_node.n+=1
        curr_node.v+=reward
        curr_node = curr_node.parent
    curr_node.n+=1
    curr_node.v+=reward
    return curr_node

def mcts_pred(curr_node, iterations):

    all_moves = curr_node.state.legal_moves()
    map_state_move = dict()
    list_effect_child = []
    for move in all_moves:

        tmp_state = copy.deepcopy(curr_node.state)
        tmp_state.play_move(move)
        child = Node()
        child.state = tmp_state
        child.parent = curr_node
        curr_node.children.append(child)
        map_state_move[child] = move

    while (iterations > 0):

        idx = -1
        max_ucb = -inf
        sel_child = None
        random.shuffle(curr_node.children)
        for i in curr_node.children:
            tmp = ucb1(i)
            if (tmp > max_ucb):
                idx = i
                max_ucb = tmp
                sel_child = i
        ex_child = expand(sel_child)
        reward, state = rollout(ex_child)
        curr_node = rollback(state, reward)
        list_effect_child.append(sel_child)
        iterations -= 1

    mx = -inf
    idx = -1
    selected_move = ''
    for child in (list_effect_child):
        tmp = ucb1(child)
        print(f"tmp for child {tmp}")
        if (tmp > mx):
            mx = tmp
            selected_move = map_state_move[child]
    return selected_move

def mcts_NN_pred(curr_node, iterations):
    # TODO: replace this statement with all moves from Goban
    # all_moves = [curr_node.state.san(i) for i in list(curr_node.state.legal_moves)]
    all_moves = curr_node.state.legal_moves()  # legal moves are given as internal (flat) coordinates, not A1, A2, ...
    # all_moves_name = [curr_node.state.flat_to_name(l) for l in all_moves]
    map_state_move = dict()
    list_effect_child = []
    for move in all_moves:
        # TODO: adapt to Board to create new board
        # tmp_state = Chess.Board(curr_node.state.fen())
        # tmp_state.push_san(i)
        tmp_state = copy.deepcopy(curr_node.state)
        tmp_state.play_move(move)
        child = Node()
        child.state = tmp_state
        child.parent = curr_node
        curr_node.children.append(child)
        map_state_move[child] = move

    while (iterations > 0):

        idx = -1
        max_ucb = -inf
        sel_child = None
        random.shuffle(curr_node.children)
        for i in curr_node.children:
            tmp = ucb1(i)
            if (tmp > max_ucb):
                idx = i
                max_ucb = tmp
                sel_child = i
        ex_child = expand(sel_child)
        reward, state = rollout(ex_child)
        curr_node = rollback(state, reward)
        list_effect_child.append(sel_child)
        iterations -= 1

    mx = -inf
    idx = -1
    selected_move = ''
    for child in (list_effect_child):
        tmp = ucb1(child)
        print(f"tmp for child {tmp}")
        if (tmp > mx):
            mx = tmp
            selected_move = map_state_move[child]
    return selected_move