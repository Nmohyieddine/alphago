#!/bin/bash

mkdir downloads
cd downloads
# Get a list of SGF archive file URLs
wget  https://homepages.cwi.nl/~aeb/go/games/9x9.tgz

echo "Done fetching archive files. Extracting..."
cd ..
mkdir SGFs
cd SGFs

for archive in ../downloads/*.tgz ; do
    tar zxvf "$archive"
done

echo "Done!"

