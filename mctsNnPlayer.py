import Goban
from node import Node
from playerInterface import *
from model.model import Residual_CNN
import model.config as config
class myPlayer(PlayerInterface):

    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None

    def getPlayerName(self):
        return "MCTS NN Player"

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"
        moves = self._board.legal_moves()
        print(f"the possible move {moves}")
        node = Node()
        node.state = self._board


        best_NN = Residual_CNN(config.REG_CONST, config.LEARNING_RATE, (2,) + (9, 9), len(moves), config.HIDDEN_CNN_LAYERS)

        result =


    def playOpponentMove(self, move):
        '''Inform you that the oponent has played this move. You must play it with no
        search (just update your local variables to take it into account)

        The move is given as a GO move string, like "A1", ... "J9", "PASS"

        WARNING: because the method Goban.push(m) needs a move represented as a flat move (integers),
        you can not directly call this method with the given move here. You will typically call
        b.push(Board.name_to_flat(move)) to translate the move into its flat (internal) representation.
         '''
        pass

    def newGame(self, color):
        '''Starts a new game, and give you your color.  As defined in Goban.py : color=1
        for BLACK, and color=2 for WHITE'''
        pass

    def endGame(self, color):
        '''You can get a feedback on the winner
        This function gives you the color of the winner'''
        pass