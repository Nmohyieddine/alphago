import Goban
from playerInterface import *


from node import Node, mcts_pred


class myPlayer(PlayerInterface):

    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None

    def getPlayerName(self):
        return "MCTS Player"

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"
        # Get the list of all possible moves
        moves = self._board.legal_moves() # Dont use weak_legal_moves() here!
        print(f" the posible move {moves}")
        node = Node()
        node.state = self._board
        result = mcts_pred(node,10)
        self._board.play_move(result)

        return node.state.flat_to_name(result)

    def playOpponentMove(self, move):
        #print("Opponent played ", move, "i.e. ", move) # New here
        # the board needs an internal represetation to push the move.  Not a string
        self._board.push(Goban.Board.name_to_flat(move))

    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")