import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self, game, args):
        super(Net, self).__init__()
        # game params
        self.board_x, self.board_y = game.getBoardSize()
        self.action_size = game.getActionSize()
        self.args = args

        # Neural Net
        self.conv1 = nn.Conv2d(1, args.num_channels, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(args.num_channels, args.num_channels, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(args.num_channels, args.num_channels, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(args.num_channels, args.num_channels, kernel_size=3, padding=1)
        self.fc1 = nn.Linear(args.num_channels * (self.board_x - 4) * (self.board_y - 4), 1024)
        self.fc2 = nn.Linear(1024, 512)
        self.fc3 = nn.Linear(512, self.action_size)
        self.fc4 = nn.Linear(512, 1)
        self.dropout = nn.Dropout(p=args.dropout)

    def forward(self, x):
        x = x.view(-1, 1, self.board_x, self.board_y)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        x = x.view(-1, args.num_channels * (self.board_x - 4) * (self.board_y - 4))
        x = self.dropout(F.relu(self.fc1(x)))
        x = self.dropout(F.relu(self.fc2(x)))
        pi = F.softmax(self.fc3(x), dim=1)
        v = torch.tanh(self.fc4(x))
        return pi, v