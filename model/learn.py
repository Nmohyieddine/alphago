import sys

from rts.src.config_class import CONFIG

sys.path.append('..')
from NNet import NNetWrapper
sgf_path = "/home/mohyi/Desktop/Option IA 2022-2023/alphago/data/KGS/SGFs/9x9/computer/CC1.sgf"
from sgfmill import sgf
def extraxt_player_actions(sgf_path):
    with open(sgf_path, "rb") as f:
        game = sgf.Sgf_game.from_bytes(f.read())
    winner = game.get_winner()
    board_size = game.get_size()
    root_node = game.get_root()
    b_player = root_node.get("PB")
    w_player = root_node.get("PW")
    for node in game.get_main_sequence():
        print(node.get_move())

    return game.get_main_sequence(), board_size, winner

if __name__ == "__main__":

    # extract the data from dataset using extract_player_actions
    game_sequence, board_size, winner = extraxt_player_actions(sgf_path)

    # create a game with this game_sequence in order to fianly get [pi, v, board] list using namedGame


    # train the model using this list
    nn = NNetWrapper()
    nn.train()
    # save model to use it in prediction
    nn.save_checkpoint()




